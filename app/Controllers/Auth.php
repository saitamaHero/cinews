<?php

namespace App\Controllers;

use \Firebase\JWT\JWT;

class Auth extends BaseController
{
    use \CodeIgniter\API\ResponseTrait;

    public $validLogins =
    [];

    protected $key = "s0HFbeVuUIXPTVZk78eotrK7npFUddXF";


    public function login()
    {
        $this->validLogins[] = ['email' => 'dionicioacevedo@lotoreal.com.do', 'username' => 'dionicioacevedo', 'password' => sha1('12345678')];
        $this->validLogins[] = ['email' => 'joseolivo@lotoreal.com.do', 'username' => 'joseolivo', 'password' => sha1('lapampara')];
        $this->validLogins[] = ['email' => 'raulcarpio@lotoreal.com.do', 'username' => 'raulcarpio', 'password' => sha1('thepampara')];

        $rawInput = $this->request->getJSON();

        if (isset($rawInput->user_or_email) && isset($rawInput->password)) {
            foreach ($this->validLogins as $user) {
                if (strcasecmp($user['email'],$rawInput->user_or_email) == 0 || strcasecmp($user['username'],$rawInput->user_or_email) == 0) {

                    if (strcmp($user['password'],sha1($rawInput->password)) == 0) {

                        $expirationTime = 86400;
                        $now = new \DateTime('now');
                        $iat = $now->getTimestamp();
                        $exp = $iat + $expirationTime;


                        $payload = array(
                            "iat" => $iat,
                            "exp" => $exp
                        );

                        unset($user['password']);

                        $payload['user'] = $user;

                        $jwt = JWT::encode($payload, $this->key);

                        $user['token'] = $jwt;


                        return $this->respond($user);
                    }
                }
            }
        } else {
            return $this->fail(['message' => ['The user and password missing in the request body'], 'body' => $rawInput],  400, null, '');
        }


        return $this->failNotFound("User or Password is incorrect!".json_encode($rawInput), null);
    }
}
