<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;

class Users extends Controller
{
    public function index()
    {
        $users = [
            ['username' => 'dionicioa', 'role' => 'USER_ROLE', 'name' => 'Dionicio Acevedo'],
            ['username' => 'dionicioa', 'role' => 'USER_ROLE', 'name' => 'Dionicio Acevedo'],
            ['username' => 'dionicioa', 'role' => 'USER_ROLE', 'name' => 'Dionicio Acevedo'],
        ];


        $response = [
            'status'   => 400,
            'code'     => '321a',
            'messages' => [
                'Error message 1',
                'Error message 2'
            ]
        ];

        //return $this->fail($response);


        //return $this->response->setJSON($users);
    }
}