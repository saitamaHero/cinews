<?php

namespace App\Controllers;

use DateTime;
use DateTimeZone;
use Exception;
use \Firebase\JWT\JWT;

class Home extends BaseController
{
	public function index()
	{
	//	echo date_default_timezone_get();


		return view('welcome_message');	
	}

	//--------------------------------------------------------------------

	public function test()
	{
		$expirationTime = 86400;
		$now = new DateTime('now');
		$key = "s0HFbeVuUIXPTVZk78eotrK7npFUddXF";
		$iat = $now->getTimestamp();
		$exp = $iat + $expirationTime;


		$payload = array(
			"iat" => $iat,
			"exp" => $exp
		);

		$user = [];
		$user['username']  = 'dionicioa';
		$user['email']     = 'dionicioacevedo@lotoreal.com.do';
		$user['user_role'] = 'user_role';
		$user['login_at']  = (new DateTime())->format(DateTime::ISO8601);

		$payload['user'] = $user;

		/**
		 * IMPORTANT:
		 * You must specify supported algorithms for your application. See
		 * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
		 * for a list of spec-compliant algorithms.
		 */
		
		$jwt = JWT::encode($payload, $key);

		return $this->response->setJSON(['token' => $jwt]);
	}

	public function food()
	{
		$token = $this->request->getGet('token');

		//echo 'token: '.$token;
//		echo 'tz:'. datefmt_get_timezone();
		$key = "s0HFbeVuUIXPTVZk78eotrK7npFUddXF";

		try {
			$data = JWT::decode($token, $key, array('HS256'));

			if ($data) {
				$user = $data->user;
				echo 'Yor are login: ' . $user->email;
				
			}
		} catch (\UnexpectedValueException $ex1) {
			echo 'Token Invalido: '.$ex1->getMessage();
		} catch (\SignatureInvalidException $ex2) {
			echo 'Firma falsificada';
		} catch (\ExpiredException $ex3) {
			echo 'Inicia Sessión Nuevamente';
		} catch (Exception $e) {
			echo 'Yor are login: ' . $data['user']['email'];
		}
	}
}
